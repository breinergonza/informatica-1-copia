package co.dev.sqdm.hexeditor;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    private Button btn_cargar;
    private ToggleButton btn_endian;
    private EditText txt_hexa,txt_char;
    private TextView txt_8bits,txt_8bits_signo,txt_16bits,txt_32bits,txt_64bits,txt_32bits_float,txt_64bits_float,txt_offset;
    private int READ_REQUEST_CODE =23;
    String tag = MainActivity.class.getName();
    List<String> stringListHex = new ArrayList<>();
    List<Character> stringListChar = new ArrayList<>();
    ArrayList<String> list_8_Bytes=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_cargar = findViewById(R.id.btn_cargar);
        txt_hexa = findViewById(R.id.txt_hexa);
        txt_char = findViewById(R.id.txt_char);
        txt_8bits = findViewById(R.id.txt_8bits);
        txt_8bits_signo = findViewById(R.id.txt_8bits_signo);
        txt_16bits = findViewById(R.id.txt_16bits);
        txt_32bits = findViewById(R.id.txt_32bits);
        txt_64bits = findViewById(R.id.txt_64bits);
        txt_32bits_float = findViewById(R.id.txt_32bits_float);
        txt_64bits_float = findViewById(R.id.txt_64bits_float);
        btn_endian = findViewById(R.id.btn_endian);
        txt_offset = findViewById(R.id.txt_offset);
        txt_hexa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String enteredText = txt_hexa.getText().toString();
                int cursorPosition = txt_hexa.getSelectionStart();
                String enteredTextCursor=enteredText.substring(0,cursorPosition);
                int occurences = enteredTextCursor.length() - enteredTextCursor.replace("\n", "").length();
                cursorPosition = cursorPosition-occurences;
                enteredText = enteredText.replaceAll("\n","");
                list_8_Bytes=new ArrayList<>();
                if (enteredText.substring(cursorPosition-1,cursorPosition).equalsIgnoreCase(" ")
                    && !enteredText.substring(cursorPosition,cursorPosition+1).equalsIgnoreCase(" "))
                {
                    list_8_Bytes.add(enteredText.substring(cursorPosition,cursorPosition+2));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+3,cursorPosition+5));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+6,cursorPosition+8));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+9,cursorPosition+11));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+12,cursorPosition+14));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+15,cursorPosition+17));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+18,cursorPosition+20));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+21,cursorPosition+23));
                    Toast.makeText(MainActivity.this,enteredText.substring(cursorPosition,cursorPosition+2),Toast.LENGTH_SHORT).show();
                }
                else if (!enteredText.substring(cursorPosition-1,cursorPosition).equalsIgnoreCase(" ")
                        && !enteredText.substring(cursorPosition,cursorPosition+1).equalsIgnoreCase(" "))
                {
                    list_8_Bytes.add(enteredText.substring(cursorPosition-1,cursorPosition+1));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+2,cursorPosition+4));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+5,cursorPosition+7));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+8,cursorPosition+10));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+11,cursorPosition+13));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+14,cursorPosition+16));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+17,cursorPosition+19));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+20,cursorPosition+22));

                    Toast.makeText(MainActivity.this,enteredText.substring(cursorPosition-1,cursorPosition+1),Toast.LENGTH_SHORT).show();
                }
                else if (!enteredText.substring(cursorPosition-1,cursorPosition).equalsIgnoreCase(" ")
                        && enteredText.substring(cursorPosition,cursorPosition+1).equalsIgnoreCase(" "))
                {
                    list_8_Bytes.add(enteredText.substring(cursorPosition-2,cursorPosition));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+1,cursorPosition+3));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+4,cursorPosition+6));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+7,cursorPosition+9));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+10,cursorPosition+12));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+13,cursorPosition+15));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+16,cursorPosition+18));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+19,cursorPosition+21));

                    Toast.makeText(MainActivity.this,enteredText.substring(cursorPosition-2,cursorPosition),Toast.LENGTH_SHORT).show();
                }
                convertions();
            }
        });

        btn_cargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, READ_REQUEST_CODE);
            }
        });
        btn_endian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convertions();
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i(tag, "Uri: " + uri.toString());
                try {
//                    txt_char.setText((Html.fromHtml(readTextFromUri(uri))));
                    txt_char.setText(open2(uri));
                    txt_hexa.setText(open(uri));
                    Log.i(tag, "TEXT: " + readTextFromUri(uri));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private String readTextFromUri(Uri uri) throws IOException {
//        Scanner scanner = new Scanner(new File(uri)).useDelimiter("\u001F");
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream,"ISO-8859-1"));
        StringBuilder stringBuilder = new StringBuilder();
        int line;
        while ((line = reader.read()) != -1) {
            stringBuilder.append((char)line);
        }
        String a = stringBuilder.toString();
        return stringBuilder.toString();
    }
    private String readTextFromUri2(Uri uri) throws IOException {
//        Scanner scanner = new Scanner(new File(uri)).useDelimiter("\u001F");
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream,"ISO-8859-1"));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
                String sText = line;
                String sTextAsHex = "";
                for(int nIter = 0; nIter < sText.length(); nIter++)
                {
                    //Convert ascii to hex
                    String sHex = String.format("%02x", (int)sText.charAt(nIter));
                    sHex = sHex.toUpperCase();
                    sTextAsHex += sHex;
                }
                stringBuilder.append(sTextAsHex);
            }
        String a = stringBuilder.toString();
        return stringBuilder.toString();
    }
    private String Scanner(Uri uri) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            DataInputStream textFileStream = new DataInputStream(inputStream);
            Scanner sc = new Scanner(textFileStream);

            while (sc.hasNextLine()) {
                String sText = sc.nextLine();
                String sTextAsHex = "";
                for(int nIter = 0; nIter < sText.length(); nIter++)
                {
                    //Convert ascii to hex
                    String sHex = String.format("%02x", (int)sText.charAt(nIter));
                    sHex = sHex.toUpperCase();
                    sTextAsHex += sHex;
                }
                stringBuilder.append(sTextAsHex);
            }
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
    public String open( Uri uri ) throws IOException
    {
        InputStream is  = getContentResolver().openInputStream(uri);
        stringListHex = new ArrayList<>();
        int bytesCounter = 0;
        int value = 0;
        long offsetCounter = 1;
        StringBuilder sbHex = new StringBuilder();
        StringBuilder sbHexOffset = new StringBuilder();
        StringBuilder sbResult = new StringBuilder();
        sbHexOffset.append(offsetCounter);
        while ((value = is.read()) != -1) {
            //convert to hex value with "X" formatter
            sbHex.append(String.format("%02X ", value));

            stringListHex.add(String.format("%02X ", value));
            //if 16 bytes are read, reset the counter,
            //clear the StringBuilder for formatting purpose only.
            offsetCounter++;
            if (bytesCounter == 15) {
                sbResult.append(sbHex).append("\n");
                sbHex.setLength(0);
                bytesCounter = 0;
                sbHexOffset.append("\n");
                sbHexOffset.append(offsetCounter);

            } else {
                bytesCounter++;
            }

        }

        //if still got content
        if (bytesCounter != 0) {
            //add spaces more formatting purpose only
            for (; bytesCounter < 16; bytesCounter++) {
                //1 character 3 spaces
                sbHex.append("   ");
            }
            sbResult.append(sbHex).append("\n");
        }
        is.close();
        txt_offset.setText(sbHexOffset);
        return sbResult.toString();
    }
    public String open2( Uri uri ) throws IOException
    {
        stringListChar = new ArrayList<>();
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream,"US-ASCII"));
        StringBuilder stringBuilder = new StringBuilder();
        int line;
        while ((line = inputStream.read()) != -1) {
            if (line >= 33 && line <= 126) {
                stringBuilder.append((char)line);
                stringListChar.add((char)line);
            }
            else {
                stringBuilder.append(".");
                stringListChar.add('.');
            }

        }

        return stringBuilder.toString();
    }
    private void convertions(){
//        Collections.reverse(list_8_Bytes);
        if (!list_8_Bytes.isEmpty()){
            if (!btn_endian.isChecked()) {//Big Endian
                String bits_32_str = list_8_Bytes.get(0) + list_8_Bytes.get(1) +
                        list_8_Bytes.get(2) + list_8_Bytes.get(3);
                String bits_64_str = list_8_Bytes.get(0) + list_8_Bytes.get(1) +
                        list_8_Bytes.get(2) + list_8_Bytes.get(3) +
                        list_8_Bytes.get(4) + list_8_Bytes.get(5) +
                        list_8_Bytes.get(6) + list_8_Bytes.get(7);
                int bits_8 = Integer.parseInt(list_8_Bytes.get(0), 16);
                int bits_8_signed = (byte) Integer.parseInt(list_8_Bytes.get(0), 16);
                Float bits_32_float = Float.intBitsToFloat(Long.valueOf(bits_32_str, 16).intValue());
                Double bits_64_double = Double.longBitsToDouble(new BigInteger(bits_64_str, 16).longValue());
                int bits_16 = Integer.parseInt((list_8_Bytes.get(0) + list_8_Bytes.get(1)).replace(" ", ""), 16);
                BigInteger bits_32 = new BigInteger((bits_32_str).replace(" ", ""), 16);
                BigInteger bits_64 = new BigInteger((bits_64_str).replace(" ", ""), 16);

                txt_8bits.setText(String.valueOf(bits_8));
                txt_8bits_signo.setText(String.valueOf(bits_8_signed));
                txt_16bits.setText(String.valueOf(bits_16));
                txt_32bits.setText(String.valueOf(bits_32));
                txt_64bits.setText(String.valueOf(bits_64));
                txt_32bits_float.setText(String.valueOf(bits_32_float));
                txt_64bits_float.setText(String.valueOf(bits_64_double));
            }
            else{
                String bits_32_str = list_8_Bytes.get(3) + list_8_Bytes.get(2) +
                        list_8_Bytes.get(1) + list_8_Bytes.get(0);
                String bits_64_str = list_8_Bytes.get(7) + list_8_Bytes.get(6) +
                        list_8_Bytes.get(5) + list_8_Bytes.get(4) +
                        list_8_Bytes.get(3) + list_8_Bytes.get(2) +
                        list_8_Bytes.get(1) + list_8_Bytes.get(0);
                int bits_8 = Integer.parseInt(list_8_Bytes.get(0), 16);
                int bits_8_signed = (byte) Integer.parseInt(list_8_Bytes.get(0), 16);
                Float bits_32_float = Float.intBitsToFloat(Long.valueOf(bits_32_str, 16).intValue());
                Double bits_64_double = Double.longBitsToDouble(new BigInteger(bits_64_str, 16).longValue());
                int bits_16 = Integer.parseInt((list_8_Bytes.get(1) + list_8_Bytes.get(0)).replace(" ", ""), 16);
                BigInteger bits_32 = new BigInteger((bits_32_str).replace(" ", ""), 16);
                BigInteger bits_64 = new BigInteger((bits_64_str).replace(" ", ""), 16);

                txt_8bits.setText(String.valueOf(bits_8));
                txt_8bits_signo.setText(String.valueOf(bits_8_signed));
                txt_16bits.setText(String.valueOf(bits_16));
                txt_32bits.setText(String.valueOf(bits_32));
                txt_64bits.setText(String.valueOf(bits_64));
                txt_32bits_float.setText(String.valueOf(bits_32_float));
                txt_64bits_float.setText(String.valueOf(bits_64_double));
            }

        }

    }


}
